# Nombre d'accidents selon les catégories de véhicules mises en cause

Ce module permet de calculer le nombre de victimes selon la gravité
(blessés légers, blessés graves et décédés), la catégorie du véhicule
de la victime et la catégorie du ou des autres véhicules impliqués
dans l'accident.

Vous fabriquer un graphe similaire à celui représenté sur
l'infographie (et les données correspondantes) avec les commandes
`python3` suivantes :

```python
import graphes
graphes.fabrique("loi_du_plus_lourd")
```

Vous trouverez alors le résultat dans le répertoire
`resultats/loi_du_plus_lourd`.

Vous pouvez adapter ces résultats à d'autres contextes, grâce aux
options suivantes. En guise d'exemple, si vous ne vous intéressez qu'à
la Bretagne (départements 22, 29, 35 et 56) sur les années 2018 à
2020, avec un affichage des cumuls en effectifs absolus, utilisez

```python
import graphes
graphes.fabrique("loi_du_plus_lourd",
                 region = ['22','29','35','56'],
                 annees = [2018, 2019, 2020],
                 pourcentage = False)
```

* `annees` : liste des années de l'étude. La valeur par défaut est
  `(2015, 2016, 2017, 2018, 2019, 2020)`.

* `region` : une région dans laquelle restreindre l'étude, sous la
  forme d'une liste de débuts de codes INSEE des communes à
  considérer. La valeur par défaut (`None`) sélectionne la France
  entière. Pour choisir de se restreindre aux communes du Rhône (y
  compris la métropole de Lyon), on peut choisir `['69']`. Pour l'Île
  de France, `['75','77','78','91','92','93','94','95']`.

* `mult_action` : permet de choisir ce qu'il faut faire quand
  l'accident implique plus de deux véhicules.

  * Avec le choix `mult_action='repartition'` (c'est la valeur par
    défaut), une victime pour laquelle n véhicules (autres que le
    sien) sont impliqués dans l'accident sera comptée à hauteur de
    1/n dans chacune des catégories concernées.

  * Avec le choix `mult_action='a_part'`, une catégorie nommée "MULT"
    sera créée pour compter les cas où plusieurs catégories de
    véhicules différentes interviennent (en plus de celle de la
    victime) dans l'accident.

* `regroupe_categories` : permet de regrouper des catégories de
  véhicules. La valeur par défaut est la suivante :
```python
  { 'autres' : ('X', 'S', '4RM', 'TR', 'EDP'),
    '2/3RM': ('2RM', '3RM'),
    'PL/TC' : ('PL', 'TC') }
```

* `couleurs_gravite` : dictionnaire qui permet de définir les couleurs
  à utiliser pour chacun des niveaux de gravite. Par défaut, sa valeur
  est `{2:'#800000', 3:'#FF6600', 4:'#FFCC00'}`.

* `cats` : liste des catégories de victimes, dans l'ordre de leur
  affichage souhaité. Par défaut, essaye de faire au mieux…

* `visavis` : liste des catégories de véhicules impliqués, dans
  l'ordre de leur affichage souhaité. Par défaut, essaye de faire au
  mieux…

* `taillemax` : diamètre du cercle le plus grand, étant donné que deux
  cercles voisins sont distants de 1. La valeur par défaut est 1.5, ce
  qui peut donner des cercles qui se recouvrent mais permet une
  meilleure lisibilité.

* `size` : distance entre deux cercles en pouces (inches).

* `gravn` : le niveau de gravité pour lequel on souhaite afficher les
  nombres sur chaque cercle. Avec la valeur `None` par défaut, ne
  produit aucun affichage.

* `pourcentage` : indique si on souhaite afficher les cumuls en
  pourcentage (avec la valeur `True` par défaut), ou bien les
  effectifs (avec la valeur `False`).

* `ch_sign` : le nombre de chiffres significatifs à retenir lors de
  l'affichage des nombres de blessés. Une valeur négative permet de
  garder tous les chiffres (pas d'arrondi). La valeur par défaut est
  2.

* `par_annee` : indique s'il faut diviser les totaux de nombres
  d'accidents par le nombre d'années prises en compte (actif par
  défaut).