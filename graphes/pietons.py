
import data.baac as baac
from statsmodels.graphics.mosaicplot import mosaic
import matplotlib
import matplotlib.pyplot as plt
import pandas as pd
import math

"""Qui tue les piétons sur les passages protégés et les trottoirs ?

Ce module permet de recenser le nombre de piétons renversés alors
qu'ils traversent sur un passage protégé (avec les paramètres par
défaut), ou lorsqu'ils sont sur un trottoir, en fonction de la
catégorie du véhicule qui l'a percuté.

"""

class Graphe:

    """Attributs
    ---------
    annees : array
      Liste des années de l'étude

    region : array
      Une région dans laquelle restreindre l'étude (voir
      data.baac.selectionne_region).

    locp : array
      La liste des codes de localisation du piéton considérés. Par
      exemple, 3 et 4 correspondent aux passages piétons
      (respectivement sans et avec signalisation lumineuse).

    unite : int
      Le nombre de blessés qui correspondent à un cercle complet dans
      le graphique

    hauteur : int
      Le nombre de cercles dans une colonne du graphique

    regroupe_categories : dict
      Un dictionnaire décrivant les regroupements de catégories à effectuer.

    couleurs_gravite : dict
      Un dictionnaire qui permet d'affecter une couleur à chaque
      niveau de gravité.

    pp : DataFrame
      Contient une ligne par piéton blessé sur un passage protégé,
      avec toutes les variables contenues dans le fichier usagers du
      BAAC, ainsi que la colonne catvsimple qui correspond à la
      catégorie du véhicule qui a renversé le piéton.

    bilan : DataFrame
      Contient le décompte des piétons blessés, par gravité de
      l'accident et catégorie du véhicule impliqué.

    """

    annees = (2019,)
    region = None
    locp = (3, 4)

    unite = 10
    hauteur = 10
    regroupe_categories = { 'O' : ('X', 'S', '3RM', '4RM', 'TR', 'EDP'),
                            'PL/TC' : ('PL', 'TC') }
    couleurs_gravite = {'2':'#800000', '3':'#FF6600', '4':'#FFCC00'}
                 
    pp = None
    bilan = None

    def __init__(self, **params):
        """Paramètres nommés
        -----------------

        annees, region, locp, unite, hauteur, regroupe_categories,
        couleurs_gravite : voir les attributs de classe.

        """
        self.change_parametres(**params)

    def change_parametres(self, **params):
        """Changement des paramètres

        Change les paramètres, et recalcule des DataFrame pp et bilan.

        Paramètres nommés
        -----------------

        annees, region, locp, unite, hauteur, regroupe_categories,
        couleurs_gravite : voir les attributs de classe.

        """
        for k,v in params.items():
            if k in ['annees', 'region', 'unite', 'hauteur', 'locp', 
                     'regroupe_categories', 'couleurs_gravite']:
                setattr(self, k, v)

        recalc = False
        for k in ['annees', 'region', 'locp', 'regroupe_categories']:
            if k in params:
                recalc = True

        if recalc or self.pp is None:
            self.calcule()

    def calcule(self):
        """Calculs

        Lecture des bases de données et calculs (DataFrame pp et bilan).

        """
        # Part d'une DataFrame vide,
        self.pp = pd.DataFrame()

        # et, pour chaque année
        for a in self.annees:
            # récupère la liste des piétons blessés ;
            p = baac.victimes_pietons(annee = a, region = self.region)

            # les sélectionne suivant leur,ocalisation ;
            p = p[ p["locp"].isin(self.locp) ]

            # et ajoute le résultat à la liste.
            self.pp = self.pp.append(p)
            
        # Regroupe les catégories suivant les paramètres
        for k, v in self.regroupe_categories.items():
            self.pp.loc[self.pp['catvsimple'].isin(v),
                        'catvsimple'] = k

        # Compte le nombre de victimes piétonnes, par gravité et véhicule
        # concerné
        self.bilan = self.pp.groupby(['grav','catvsimple']) \
            ['id_vehicule'].count().reset_index(name="n")

    def data(self, fichier):
        """Sauvegarde des données dans un fichier CSV

        Paramètres
        ----------
        fichier : str
            La base du chemin du fichier à enregistrer.
        """
        self.bilan.to_csv(fichier + '.csv', index=False)

    def nb(self, catvsimple, grav):
        """Nombre correspondant à un couple catégorie/gravité

        Renvoie le nombre de piétons renversés par un véhicule de la
        catégorie donnée, et ayant été touché avec une gravité donnée.

        Paramètres
        ----------
        catvsimple : str
            La catégorie des véhicules qui ont renversés des piétons
        
        grav : integer
            La gravité avec laquelle le piéton a été touché (2=mort,
            3=hôpital, 4=blessé)

        Retour
        ------
        Le nombre de piétons blessés correspondant.

        """
        return self.bilan.loc[(self.bilan["grav"]==int(grav))&
                              (self.bilan["catvsimple"]==catvsimple), "n"] \
                         .values.sum()

    def dessine_cercles(self, cat, x0=0, dx=1.1):
        """Dessine les cercles pour une catégorie

        Dessine les cercles du graphique correspondant à une catégorie
        de véhicules donnée, en commençant à l'absice x0.

        Paramètres
        ----------
        cat : str
            La catégorie de véhicules concernée.

        x0 : float
            L'absice où commencer le dessin.

        dx : float
            La distance entre deux cercles (les cercles pleins ayant
            un diamètre égal à 1).

        Retour
        ------
        (x1, h), où x1 est la dernière position x utilisée, et h la
        hauteur en nombre de cercles.

        """
        h = self.hauteur
        if h < 1:
            n = 0
            for i in (2,3,4):
                n += self.nb(cat, i)
            h = math.ceil(math.sqrt(n / self.unite))

        # Position de départ
        x = x0
        y = 0
        commence = 0

        for i in (2,3,4):
            n = self.nb(cat, i)
            while n > 0:
                # Change de cercle si besoin
                if commence == self.unite:
                    commence = 0
                    y += 1
                    if y >= h:
                        y = 0
                        x += 1

                # Compte le nombre d'unités à dessiner
                ajoute = min(self.unite - commence, n)

                # Dessine
                if ajoute == self.unite:
                    # Cercle complet
                    c=matplotlib.patches.Circle( (x*dx, -y*dx),
                                  radius=1/2, linewidth=0,
                                  color=self.couleurs_gravite[str(i)] )
                else:
                    # Secteur
                    c = matplotlib.patches.Wedge( (x*dx, -y*dx),
                                   theta1=90+commence*360/self.unite,
                                   theta2=90+(commence+ajoute)*360/self.unite,
                                   r=1/2, linewidth=0,
                                   color=self.couleurs_gravite[str(i)] )
                plt.gca().add_artist(c)
                
                # Passe à la suite
                n -= ajoute
                commence += ajoute
        return x, h

    def dessine_groupes_cercles(self, dx=1.1, eps=0.1, dgroupe=2, size=0.1):
        """Dessine la figure complète des cercles des blessés

        Paramètres
        ----------
        dx : float
            Espace entre deux cercles voisins.

        eps : float
            Marge autour du dessin.

        dgroupe : float
            Espace entre deux groupes de cercles (correspondant à des
            catégories différentes).

        size : float
            Diamètre d'un cercle en pouces (inches).

        """
        plt.close('all')
        plt.axis('off')

        x0=0
        hmax=0

        # Récupère la liste des catégories, par ordre décroissant du
        # nombre total de blessés
        categories = list(self.bilan.groupby(['catvsimple']) \
                          .aggregate(n=('n','sum')).reset_index() \
                          .sort_values('n',ascending=False)['catvsimple'])
        
        for c in categories:
            if c != '*':
                x1,h = self.dessine_cercles(c, x0=x0, dx=dx)
                plt.text((x0+x1)/2*dx, 2, c,
                         horizontalalignment='center',
                         verticalalignment='top')
                x0 = x1 + dgroupe/dx
                if h > hmax:
                    hmax = h

        xmin, xmax = -0.5-eps, x0*dx-dgroupe+0.5+eps
        ymin, ymax = -(hmax-1)*dx-0.5-eps, 2+eps
        plt.xlim(xmin, xmax)
        plt.ylim(ymin, ymax)

        plt.gcf().set_size_inches(size*(xmax-xmin), size*(ymax-ymin))
        plt.subplots_adjust(left=0, bottom=0, right=1, top=1)

    def graphe(self, fichier):
        """Production et enregistrement des graphes

        Produit deux graphes représentant le nombre de piétons blessés
        sur des passages protégés, en fonction de la gravité de
        l'accident et du véhicule concerné.

        * Un graphe « mosaïque », avec des rectangles imbriqués de
          surface proportionnele au nombre de cas.

        * Un graphe « cercles », avec des cercles de surface totale
          proportionnelle au nombre de cas.

        Paramètres
        ----------
        fichier : str
            La base du chemin des fichiers où enregistrer les graphes.

        """
        # Taille des textes
        plt.rc('font', size=8)
        plt.rc('xtick', labelsize=6)
        
        # Couleurs et annotations sur le graphe
        props = lambda key: {
            'color': self.couleurs_gravite[key[1]]
        }
        labelizer = lambda key: self.nb(key[0], key[1])
        
        mosaique = mosaic(self.pp,['catvsimple','grav'],
                          properties=props,
                          labelizer=labelizer)
        plt.savefig(fichier + '.jpg', dpi=300)

        # Collections de cercles pour illustrer le nombre de blessés
        # et décédés pour chaque catégorie concernée
        self.dessine_groupes_cercles()
        plt.savefig(fichier + '_cercles.svg', transparent=True)
