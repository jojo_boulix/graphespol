
import data.baac as baac
import matplotlib.pyplot as plt
import math
import pandas as pd

"""Qui blesse qui ?

Ce module permet de recenser les blessés, en fonction des catégories du
véhicule de la personne blessée et de l'autre véhicule impliqué dans
l'accident.

"""

def round_ch_sign(n, ch_sign):
    """Arrondi avec un nombre de chiffres significatifs donné

    Paramètres
    ----------
    n : int
      Le nombre à arrondir

    ch_sign : int
      Le nombre de chiffres significatifs à conserver, ou -1 si on
      veut tous les garder (pas d'arrondi).

    """
    if ch_sign < 1 or n==0:
        return n
    else:
        # nombre de chiffres de n
        nch = int(math.floor(math.log10(abs(n))))
        return round(n, ch_sign - nch - 1)

def round_pourcentage(x):
    """Arrondit un pourcentage avec si possible deux chiffres significatifs et au plus un chiffre après la virgule.
    """
    x *= 100
    if x>=10:
        return "{:.0f} %".format(x)
    else:
        return "{:.1f} %".format(x)

def int2str(n):
    """Transforme un entier en chaîne de caractères, avec espaces pour les milliers
    """
    s = str(int(n))
    s_esp = ""
    for i, c in enumerate(reversed(s)):
        if i>0 and i%3==0:
            s_esp = " " + s_esp
        s_esp = c + s_esp
    return s_esp

class Graphe:

    """Attributs
    ---------
    annees : array
      Liste des années de l'étude

    region : array
      Une région dans laquelle restreindre l'étude (voir
      data.baac.selectionne_region).

    mult_action : str
      Permet de choisir ce qu'il faut faire quand l'accident implique
      plus de deux véhicules (voir data.baac.victimes_face).

    regroupe_categories : dict
      Un dictionnaire décrivant les regroupements de catégories à effectuer.

    couleurs_gravite : dict
      Un dictionnaire qui permet d'affecter une couleur à chaque
      niveau de gravité.

    faceaface : DataFrame
      Contient, pour chaque couple (catvsimple, face_catvsimple), le
      nombre de décès concernés. catvsimple est la catégorie du
      véhicule de la personne décédée (ou "P" si c'est un piéton), et
      face_catvsimple la catégorie de l'autre véhicule impliqué dans
      l'accident, ou "0" s'il n'y en a aucun, ou "MULT" s'il y en a
      plusieurs autres de catégories différentes.

    maxncat : int
      Le nombre maximal de blessés toutes gravités pour tous les
      couples catvsimple / face_catvsimple.

    """

    annees = (2015, 2016, 2017, 2018, 2019, 2020)
    region = None
    mult_action = "repartition"
    regroupe_categories = { 'autres' : ('X', 'S', '4RM', 'TR', 'EDP'),
                            '2/3RM': ('2RM', '3RM'),
                            'PL/TC' : ('PL', 'TC') }
    couleurs_gravite = {2:'#800000', 3:'#FF6600', 4:'#FFCC00'}

    faceaface = None
    maxncat = 0

    cercles_params = {}

    def __init__(self, **params):
        """Paramètres nommés
        -----------------
        
        annees, region, mult_action, regroupe_categories,
        couleurs_gravite : voir les attributs de classe.

        """
        self.change_parametres(**params)

    def change_parametres(self, **params):
        """Changement des paramètres

        Change les paramètres, et recalcule des DataFrame pp et bilan.

        Paramètres nommés
        -----------------

        annees, region, mult_action, regroupe_categories,
        couleurs_gravite : voir les attributs de classe.

        Autres paramètres, à passer à dessine_cercles

        """
        for k,v in params.items():
            if k in ['annees', 'region', 'mult_action', 
                     'regroupe_categories', 'couleurs_gravite']:
                setattr(self, k, v)
            else:
                self.cercles_params[k] = v

        self.calcule()

    def gravites(self):
        """Liste des gravités considérées

        Retour
        ------

        La liste des gravités considérées, classées dans l'ordre
        croissant des entiers (c'est-à-dire l'ordre décroissant de
        gravité).

        """
        return sorted(self.couleurs_gravite.keys())

    def poids(self, cat):
        """Poids d'une catégorie

        Essaye d'affecter un poids à une catégorie, afin de les
        classer du moins au plus lourd.

        Paramètre
        ---------

        cat : str
          Une dénomination de catégorie.

        Retour
        ------

        Un entier représentant un poids affecté à cette catégorie.

        """
        if cat=="P":
            return 0
        elif cat=="0":
            return -10
        elif "TR" in cat:
            return 100
        elif "PL" in cat:
            return 60
        elif "TC" in cat:
            return 50
        elif "VU" in cat:
            return 40
        elif "VL" in cat:
            return 35
        elif "RM" in cat:
            return 30
        elif "V" in cat:
            return 20
        elif "EDP" in cat:
            return 10
        else:
            return 200

    def trie_categories(self, cats):
        """Trie une liste de catégories

        Trie une liste de catégories, en essayant de les ranger dans
        l'ordre croissant de poids.

        Paramètres
        ----------

        cats : array
          La liste de catégories à trier.

        Retour
        ------

        La liste triée.

        """
        return sorted(cats, key=lambda x: (self.poids(x), x))

    def calcule(self):
        """Calculs

        Lecture des bases de données et calculs (DataFrame faceaface
        et maxncat).

        """
        # Part d'une DataFrame vide,
        vic = pd.DataFrame()

        # et, pour chaque année
        for a in self.annees:
            # Rajoute les données du BAAC
            vic = vic.append(baac.victimes_face(
                annee = a,
                mult_action = self.mult_action,
                region = self.region))

        # Sélectionne les gravités
        vic = vic[vic["grav"].isin(self.gravites())]

        # Regroupe les catégories suivant les paramètres
        for k, v in self.regroupe_categories.items():
            vic.loc[vic['catvsimple'].isin(v),
                    'catvsimple'] = k
            vic.loc[vic['face_catvsimple'].isin(v),
                    'face_catvsimple'] = k

        # Calcule le nombre de blessés pour chaque couple
        # catvsimple/face_catvsimple et chaque niveau de gravité.
        self.faceaface = vic \
            .groupby(['grav','catvsimple','face_catvsimple']) \
            .aggregate(n=('poids', 'sum')) \
            .reset_index()

        # Calcule les totaux par couple catvsimple/face_catvsimple (en
        # faisant la somme sur les différentes valeurs de grav)
        total = self.faceaface \
            .groupby(['catvsimple','face_catvsimple']) \
            .aggregate(n=('n','sum')).reset_index()

        # Calcule le maximum des totaux
        self.maxncat = max([0] + total["n"])

    def data(self, fichier):
        """Sauvegarde des données dans un fichier CSV

        Paramètres
        ----------
        fichier : str
            La base du chemin du fichier à enregistrer.
        """
        self.faceaface.to_csv(fichier + '.csv', index=False)

    def nb(self, cat, facecat, grav=None):
        """Nombre de morts pour un couple catvsimple/face_catvsimple donné

        Paramètres
        ----------
        cat : str
          La catégorie des personnes décédées.

        facecat : str
          La catégorie des autres véhicules  impliqués.

        grav : int / array / None
          Le ou les niveaux de gravité considérés, ou None si on veut
          considérer tous les niveaux.

        Retour
        ------
        Le nombre de décès correspondants à ces circonstances.

        """
        # Sélection suivant catvsimple/face_catvsimple
        select = (self.faceaface["catvsimple"]==cat) & \
            (self.faceaface["face_catvsimple"]==facecat)

        # Sélection éventuelle suivant grav
        if grav is not None:
            if not isinstance(grav, list):
                grav = [ grav ]
            select = select & (self.faceaface["grav"].isin(grav))

        return self.faceaface.loc[select, "n"].values.sum()

    def dessine_cercles(self, taillemax=1.5, size=0.4,
                        cats = None,
                        visavis = None,
                        gravn = None, pourcentage = True,
                        ch_sign = 2, par_annee = True,
                        coul_lignes='#009DE360'):
        """Graphe des cercles des blessés

        Dessine des cercles dont la surface est proportionnelle au
        nombre de blessés pour chaque couple
        catvsimple/face_catvsimple et chaque niveau de gravité.

        Paramètres
        ----------

        cats : array
          La liste des catvsimple, dans l'ordre de leur affichage
          souhaité.

        visavis : array
          La liste des face_catvsimple, dans l'ordre de leur affichage
          souhaité.

        taillemax : float
            Le diamètre du cercle le plus grand, étant donné que deux
            cercles voisins sont distants de 1.

        size : float
            Distance entre deux cercles en pouces (inches).

        coul_lignes : str
            Couleur des lignes à dessiner.

        gravn : int
          Le niveau de gravité pour lequel on souhaite afficher les
          nombres pour chaque couple catvsimple/face_catvsimple

        pourcentage : bool
          Indique si on veut afficher les pourcentages, ou bien les
          effectifs.

        ch_sign : int
          Le nombre de chiffres significatifs à retenir lors de
          l'affichage des nombres de blessés. Une valeur négative
          permet de garder tous les chiffres (pas d'arrondi).

        par_annee : bool
          Indique s'il faut diviser les totaux de nombres d'accidents
          par le nombre d'années prises en compte.

        """

        plt.close('all')
        plt.axis('off')
        plt.rc('font', size=8)
        
        nmax = self.maxncat
        if cats is None:
            cats = self.trie_categories(list(
                self.faceaface["catvsimple"].unique()))
        if visavis is None:
            visavis = self.trie_categories(list(
                self.faceaface["face_catvsimple"].unique()))

        gravs = self.gravites()

        xmin, xmax = -2, (len(visavis)-1)+0.6
        ymin, ymax = -(len(cats)-1)-0.6, 1.5

        # Espaces x et y entre un bout de ligne et du texte
        sepx = 0.2
        sepy = 0.1
        # Ecartement y entre deux lignes de texte, version simple et serrée
        liney = 0.3
        liney0 = 0.28

        # Dessin des lignes
        for i,facecat in enumerate(visavis):
            line = plt.Line2D((i,i), (0.5, ymin),
                              lw=1, color=coul_lignes)
            plt.gca().add_line(line)
            
        for j,cat in enumerate(cats):
            line = plt.Line2D((-0.5, xmax), (-j,-j),
                              lw=1, color=coul_lignes)
            plt.gca().add_line(line)

        # Légende catégories de blessés
        plt.text( xmin, -(len(cats)-1)/2, "Victimes",
                  rotation = 90,
                  horizontalalignment='left',
                  verticalalignment='center' )
        for j,cat in enumerate(cats):
            plt.text( -1.5, -j, cat,
                      horizontalalignment='left',
                      verticalalignment='center' )

        # Légende véhicules percutés
        plt.text( (len(visavis)-1)/2, ymax, "Autre véhicule impliqué",
                  horizontalalignment='center',
                  verticalalignment='top' )
        for i,facecat in enumerate(visavis):
            plt.text( i, 1, facecat if facecat != '0' else 'aucun',
                      horizontalalignment='center',
                      verticalalignment='top' )

        # Taille de police pour les totaux
        plt.rc('font', size=7)

        # Cercles pour chaque couple catvsimple/face_catvsimple
        for i,facecat in enumerate(visavis):
            for j,cat in enumerate(cats):
                n = 0
                z = 10
                for g in gravs:
                    n0 = self.nb(cat, facecat, g)
                    n += n0
                    z -= 1
                    if n>0:
                        c = plt.Circle( (i,-j),
                                        radius=0.5*taillemax*math.sqrt(n/nmax),
                                        linewidth=0,
                                        color=self.couleurs_gravite[g],
                                        zorder=z)
                        plt.gca().add_patch(c)

                        # Affichage du compte pour la gravité gravn
                        if g==gravn:
                            plt.text( i + (0.1 if n<10 else 0),
                                      -j-0.3, int2str(n),
                                      horizontalalignment='center',
                                      verticalalignment='center',
                                      zorder=10)

        # Totaux genéraux par gravité
                            
        totaux_g = { 1:0, 2:0, 3:0, 4:0 }

        for i,facecat in enumerate(visavis):
            for j,cat in enumerate(cats):
                for g in gravs:
                    n0 = self.nb(cat, facecat, g)
                    totaux_g[g] += n0

        # Totaux par colonne

        for i,facecat in enumerate(visavis):
            totaux = { 1:0, 2:0, 3:0, 4:0 }
            for j,cat in enumerate(cats):
                for g in gravs:
                    n0 = self.nb(cat, facecat, g)
                    totaux[g] += n0

            for j,g in enumerate(gravs):

                if pourcentage:
                    totaux[g] = round_pourcentage( totaux[g]/totaux_g[g] )
                else:
                    if par_annee:
                        totaux[g] = int( totaux[g] / len(self.annees) )
                    totaux[g] = int2str(round_ch_sign(totaux[g], ch_sign))

                plt.text( i + (0.1 if n<10 else 0),
                          ymin - sepy - liney*j,
                          totaux[g],
                          horizontalalignment='center',
                          verticalalignment='top',
                          color=self.couleurs_gravite[g],
                          zorder=10)

        # Totaux par ligne

        for j,cat in enumerate(cats):
            totaux = { 1:0, 2:0, 3:0, 4:0 }
            for facecat in visavis:
                for g in gravs:
                    n0 = self.nb(cat, facecat, g)
                    totaux[g] += n0

            for jj,g in enumerate(gravs):
                if pourcentage:
                    totaux[g] = round_pourcentage( totaux[g]/totaux_g[g] )
                else:
                    if par_annee:
                        totaux[g] = int( totaux[g] / len(self.annees) )
                    totaux[g] = int2str(round_ch_sign(totaux[g], ch_sign))

                plt.text( xmax + sepx,
                          -j - (jj-(len(gravs)-1)/2) * liney0,
                          totaux[g],
                          horizontalalignment='left',
                          verticalalignment='center',
                          color=self.couleurs_gravite[g],
                          zorder=10)

        # Prise en compte des totaux de colonnes dans ymin
        ymin -= sepy + len(gravs) * liney
        # Et des totaux par ligne
        xmax += 1.5

        # Et marges à gauche et en haut contre les textes
        xmin -= sepx
        ymax += sepy
                
        plt.xlim(xmin, xmax)
        plt.ylim(ymin, ymax)

        plt.gcf().set_size_inches(size*(xmax-xmin),
                                  size*(ymax-ymin))
        plt.subplots_adjust(left=0, bottom=0, right=1, top=1)

    def graphe(self, fichier):
        """Production et enregistrement du graphe

        Paramètres
        ----------
        fichier : str
            La base du chemin des fichiers où enregistrer les graphes.

        Les autres paramètres sont passés à dessine_cercle.

        """
        self.dessine_cercles(**self.cercles_params)
        plt.savefig(fichier + '_cercles.svg', transparent=True)

