
import pandas as pd
from data import Sources

"""Donées BAAC

Ce module permet d'extraire de l'information des fichiers du BAAC
(accidents corporels de la circulation routière en France).

Ces fichiers sont décrits et peuvent être retrouvés sur la page
https://www.data.gouv.fr/fr/datasets/bases-de-donnees-annuelles-des-accidents-corporels-de-la-circulation-routiere-annees-de-2005-a-2020/
"""

sources = Sources(__name__)

sources.enregistre("usagers-2020.csv",
  "https://www.data.gouv.fr/fr/datasets/r/78c45763-d170-4d51-a881-e3147802d7ee")
sources.enregistre("vehicules-2020.csv",
  "https://www.data.gouv.fr/fr/datasets/r/a66be22f-c346-49af-b196-71df24702250")
sources.enregistre("lieux-2020.csv",
  "https://www.data.gouv.fr/fr/datasets/r/e85c41f7-d4ea-4faf-877f-ab69a620ce21")
sources.enregistre("caracteristiques-2020.csv",
  "https://www.data.gouv.fr/fr/datasets/r/07a88205-83c1-4123-a993-cba5331e8ae0")

sources.enregistre("usagers-2019.csv",
  "https://www.data.gouv.fr/fr/datasets/r/36b1b7b3-84b4-4901-9163-59ae8a9e3028")
sources.enregistre("vehicules-2019.csv",
  "https://www.data.gouv.fr/fr/datasets/r/780cd335-5048-4bd6-a841-105b44eb2667")
sources.enregistre("lieux-2019.csv",
  "https://www.data.gouv.fr/fr/datasets/r/2ad65965-36a1-4452-9c08-61a6c874e3e6")
sources.enregistre("caracteristiques-2019.csv",
  "https://www.data.gouv.fr/fr/datasets/r/e22ba475-45a3-46ac-a0f7-9ca9ed1e283a")

sources.enregistre("usagers-2018.csv",
  "https://www.data.gouv.fr/fr/datasets/r/72b251e1-d5e1-4c46-a1c2-c65f1b26549a")
sources.enregistre("vehicules-2018.csv",
  "https://www.data.gouv.fr/fr/datasets/r/b4aaeede-1a80-4d76-8f97-543dad479167")
sources.enregistre("lieux-2018.csv",
  "https://www.data.gouv.fr/fr/datasets/r/d9d65ca1-16a3-4ea3-b7c8-2412c92b69d9")
sources.enregistre("caracteristiques-2018.csv",
  "https://www.data.gouv.fr/fr/datasets/r/6eee0852-cbd7-447e-bd70-37c433029405")

sources.enregistre("usagers-2017.csv",
  "https://www.data.gouv.fr/fr/datasets/r/07bfe612-0ad9-48ef-92d3-f5466f8465fe")
sources.enregistre("vehicules-2017.csv",
  "https://www.data.gouv.fr/fr/datasets/r/d6103d0c-6db5-466f-b724-91cbea521533")
sources.enregistre("lieux-2017.csv",
  "https://www.data.gouv.fr/fr/datasets/r/9b76a7b6-3eef-4864-b2da-1834417e305c")
sources.enregistre("caracteristiques-2017.csv",
  "https://www.data.gouv.fr/fr/datasets/r/9a7d408b-dd72-4959-ae7d-c854ec505354")

sources.enregistre("usagers_2016.csv",
  "https://www.data.gouv.fr/fr/datasets/r/e4c6f4fe-7c68-4a1d-9bb6-b0f1f5d45526")
sources.enregistre("vehicules_2016.csv",
  "https://www.data.gouv.fr/fr/datasets/r/be2191a6-a7cd-446f-a9fc-8d698688eb9e")
sources.enregistre("lieux_2016.csv",
  "https://www.data.gouv.fr/fr/datasets/r/08b77510-39c4-4761-bf02-19457264790f")
sources.enregistre("caracteristiques_2016.csv",
  "https://www.data.gouv.fr/fr/datasets/r/96aadc9f-0b55-4e9a-a70e-c627ed97e6f7")

sources.enregistre("usagers_2015.csv",
  "https://www.data.gouv.fr/fr/datasets/r/b43a4237-9359-4217-b833-8d3dc29a6c24")
sources.enregistre("vehicules_2015.csv",
  "https://www.data.gouv.fr/fr/datasets/r/3420157e-7d23-4832-a710-a3a2f2df909c")
sources.enregistre("lieux_2015.csv",
  "https://www.data.gouv.fr/fr/datasets/r/31db21ef-4328-4c5e-bf3d-66a8fe82e6a2")
sources.enregistre("caracteristiques_2015.csv",
  "https://www.data.gouv.fr/fr/datasets/r/185fbdc7-d4c5-4522-888e-ac9550718f71")

def lecture(nom, annee=2019, catvsimple=True):
    """Lecture d'un fichier BAAC.

    Cette fonction permet de lire un fichier BAAC et d'en retourner le
    contenu sous la forme d'une DataFrame.

    Paramètres
    ----------
    nom : str
        Donne le nom du fichier recherché (qui peut être usagers,
        vehicules, caracteristiques, lieux).

    annee : integer
        Spécifie l'année civile relative aux données recherchées.

    catvsimple : boolean
        Dans le cas où le fichier contient une colonne catv (catégorie
        de véhicule), indique s'il faut lui adjoindre une catégorie
        simplifiée (en regroupant des catégories similaires) dans une
        nouvelle colonne catvsimple. La correspondance entre catv et
        catvsimple est donnée par le fichier data/baac/catvsimple.csv

    Retour
    ------
    Une DataFrame de pandas avec le contenu du fichier BAAC,
    éventuellement légèrement enrichi avec id_vehicule, catvsimple et
    INSEE.

    """
    # Lecture du fichier CSV de la BAAC
    separateur = "," if annee <= 2018 else ";"
    sepfichier = "_" if annee <= 2016 else "-"
    encoding = "iso-8859-1" if annee <= 2018 else "utf8"
    x = pd.read_csv(sources.fichier_joint(f"{nom}{sepfichier}{annee}.csv"),
                    dtype={'dep':str, 'com':str, 'voie':str},
                    sep = separateur, encoding = encoding)

    # Fabrique un identifiant de véhicule si celui-ci n'existe pas
    # déjà (c'est le cas jusqu'à 2018)
    if nom in ("usagers", "vehicules") and 'id_vehicule' not in x.columns:
        if "num_veh" in x.columns and "Num_Acc" in x.columns:
            x["id_vehicule"] = x["Num_Acc"].map(str) + "." + x["num_veh"]
        else:
            raise ValueError("Je ne peux pas créer id_vehicule")

    # Associe une catégorie de véhicules simplifiée
    if catvsimple and 'catv' in x.columns:
        simple = pd.read_csv(sources.fichier_joint("catvsimple.csv"))
        del simple["nom"]
        x = pd.merge(x, simple, how="left", on="catv")

    # Fabrique une colonne INSEE
    if nom == "caracteristiques":
        if annee >= 2019:
            x.rename(columns={"com":"INSEE"}, inplace=True)
        else:
            x.loc[ x["dep"]=="201", "dep" ] = "2A"
            x.loc[ x["dep"]=="202", "dep" ] = "2B"
            x["INSEE"] = x["dep"].str[0:2] + x["com"]

    return x

def selectionne_region(x, annee, region):
    """Sélectionne des données relatives à une région

    Paramètres
    ----------
    x : DataFrame
      La source de données. Elle doit contenir une colonne INSEE, ou,
      à défaut, une colonne Num_Acc.

    annee : integer
      L'année concernée par ces données.

    region : array
      Indique une région où restreindre l'étude (ou None si on
      souhaite les données de toute la France), sous la forme d'une
      liste de débuts de codes INSEE. Par exemple, ('69',)
      sélectionne les accidents qui ont eu lieu dans le Rhône.

    Retour
    ------
    Une DataFrame qui contient les lignes de x qui correspondent à la
    région souhaitée.

    """
    # Ajoute si nécessaire la colonne INSEE
    if 'INSEE' not in x.columns:
        # On a besoin de la colonne Num_Acc
        if 'Num_Acc' not in x.columns:
            raise KeyError("J'ai besoin de la colonne INSEE ou Num_Acc")
        # Lit la base des caractéristiques des accidents
        car = lecture("caracteristiques", annee=annee)
        # Associe la valeur de INSEE aux données
        x = pd.merge(x, car, how='left', on='Num_Acc')

    # Indique que pour l'instant aucune donnée ne correspond
    x["dans_region"] = False
    # Puis, pour chaque code de la région,
    for c in region:
        # ajoute les données concernés
        x.loc[x["INSEE"].str.startswith(str(c)),
              "dans_region"] = True

    # Enfin, supprime les données en-dehors de la région
    x = x[ x["dans_region"] ]
    return x
    
def victimes_pietons(annee=2019, region=None):
    """Liste des piétons victimes

    Cette fonction extrait la liste des piétons victimes, avec les
    caractéristiques (toutes les colonnes du fichier vehicules) du
    véhicule qui l'a percuté, sous la forme d'une DataFrame.

    Paramètres
    ----------
    annee : integer
      L'année considérée.

    region : array
      Voir selectionne_region

    Retour
    ------
    Une DataFrame contenant une ligne par piéton victime
    d'accident. Les colonnes correspondent aux variables contenues
    dans le fichier usagers du BAAC pour le piéton concerné, et aux
    variables contenues dans le fichier vehicules du BAAC pour le
    véhicule qui l'a percuté.

    """
    # Lecture de la base usagers
    usagers = lecture("usagers", annee=annee)

    # Sélection des piétons qui ne sont pas indemnes
    pietons = usagers[(usagers["catu"] == 3) & (usagers["grav"] != 1)]

    # Restriction aux blessés d'une région donnée :
    if region is not None:
        pietons = selectionne_region(pietons, annee, region)

    # Lecture de la base véhicules
    vehicules = lecture("vehicules", annee=annee)

    # Associe les caractéristiques du véhicule percutant à chaque
    # piéton
    vp = pd.merge(pietons, vehicules, how="left", on="id_vehicule")

    # Retrouve les noms de colonne initiaux pour les colonnes qui sont
    # doublonnées par le merge
    return vp.rename(columns={'Num_Acc_x':'Num_Acc', 'num_veh_x':'num_veh'})

def sort_and_join(l):
    return "+".join(sorted(l))

def compte_face(group):
    c = group['id_vehicule'].count()
    group['face_multiple'] = c
    return group

def victimes_face(annee=2020, region=None, mult_action="repartition"):
    """Liste des victimes avec catégorie du véhicule percutant/percuté

    Cette fonction retourne une liste des victimes, avec la catégorie
    du véhicule de la victime dans catvsimple (qui sera égal à "P"
    dans le cas d'un piéton), et la catégorie de l'autre véhicule
    impliqué dans cet accident dans une colonne face_catvsimple.

    Le paramètre mult_action permet de choisir ce qu'il faut faire
    quand l'accident implique plus de deux véhicules :

    * avec le choix mult_action='repartition', une victime pour
      laquelle n véhicules (autres que le sien) sont impliqués dans
      l'accident se verra affecter n lignes (une par véhicule en
      face), avec un poids de 1/n chacune

    * avec le choix mult_action='a_part', une victime correspond
      toujours à une seule lugne, et quand plusieurs véhicules en face
      ont des catégories différentes, face_catvsimple prendra la
      valeur "MULT", et face_liste contiendra les catégories de tous
      les véhicules en face.

    Si l'accident n'implique qu'un seul véhicule, qui est celui de la
    victime, alors face_catvsimple contiendra "0".

    Paramètres
    ----------
    annee : integer
        L'année considérée.

    region : array
      Indique une région où restreindre l'étude (voir
      selectionne_region).

    mult_action : str
      Voir ci-dessus.

    Retour
    ------

    Une DataFrame contenant une ligne (ou plusieurs dans le cas
    mult_action='repartition') par victime, avec des colonnes
    correspondant aux variables du fichier usagers du BAAC pour la
    victime, la colonne catvsimple qui correspond à la catégorie du
    véhicule emprunté par la victime, et une colonne face_catvsimple
    qui correspond à la catégorie de l'autre véhicule impliqué dans
    l'accident.

    """
    # Lecture de la base usagers
    usagers = lecture("usagers", annee=annee)
    usagers["face_catvsimple"] = None
    usagers["face_vehicule"] = None

    # Garde de côté les pietons avec le véhicule en face
    pietons = usagers[usagers["catu"]==3][["id_vehicule"]]
    
    # Enlève les usagers indemnes
    victimes = usagers[usagers["grav"] != 1]
    
    # Lecture de la base véhicules
    vehicules = lecture("vehicules", annee=annee)

    # Associe les caractéristiques du véhicule à chaque usager (dans
    # le cas d'un piéton c'est le véhicule percutant mais dans les
    # autres cas c'est le véhicule de la victime) :
    victimes = pd.merge(victimes, vehicules, how="left", on="id_vehicule")

    # Cas particulier des piétons (catu=3)
    victimes.loc[victimes["catu"]==3, 'face_catvsimple'] = \
        victimes.loc[victimes["catu"]==3, 'catvsimple']
    victimes.loc[victimes["catu"]==3, 'face_vehicule'] = \
        victimes.loc[victimes["catu"]==3, 'id_vehicule']
    victimes.loc[victimes["catu"]==3, ('catvsimple', 'id_vehicule')] = ('P', '0')

    # Rassemble les véhicules d'un même accident, avec une ligne par
    # couple de véhicules faisant partie d'un même accident
    crois=pd.merge(vehicules,vehicules,how="inner",on="Num_Acc") \
        [['id_vehicule_x','id_vehicule_y','catvsimple_y']]
    crois = crois[crois["id_vehicule_x"]!=crois["id_vehicule_y"]] \
        .rename(columns={'id_vehicule_x': 'id_vehicule',
                         'id_vehicule_y': 'face_vehicule',
                         'catvsimple_y': 'face_catvsimple'})

    # Ajoute les piétons en face
    pietons["face_catvsimple"] = 'P'
    pietons["face_vehicule"] = '0'
    crois = crois.append(pietons)

    if mult_action == "a_part":
        # Compte le nombre de catégories différentes impliquées en face de
        # chaque véhicule, et garde l'une d'entre elles
        crois = crois.groupby(['id_vehicule']) \
                     .aggregate(face_vehicule_a=('face_vehicule', 'first'),
                                face_multiple=('face_catvsimple', 'nunique'),
                                face_liste=('face_catvsimple', sort_and_join),
                                face_catvsimple_a=('face_catvsimple', 'first')) \
                     .reset_index()

        # Dans le cas où plusieurs catégories interviennent, utilise le
        # code "MULT"
        crois.loc[crois["face_multiple"]>1, 'face_catvsimple_a'] = "MULT"

        crois["poids"] = 1

    elif mult_action == "repartition":
        crois = crois[['id_vehicule', 'face_vehicule', 'face_catvsimple']] \
            .rename(columns={'face_vehicule': 'face_vehicule_a',
                             'face_catvsimple': 'face_catvsimple_a'})
        # Compte le nombre n de véhicules impliqués "en face" de
        # chaque véhicule
        n = crois.groupby(['id_vehicule'])['id_vehicule'].count() \
            .reset_index(name='face_multiple')
        # Et le renseigne pour chaque ligne 
        crois = pd.merge(crois, n, how="left", on="id_vehicule")
        # Enfin, affecte le poids 1/n à chaque ligne
        crois["poids"] = 1
        crois.loc[crois["face_multiple"] > 0, "poids"] = \
            1 / crois.loc[crois["face_multiple"] > 0, "face_multiple"]

    else:
        raise ValueError(f"mult_action invalide : {mult_action}") 

    # Rajoute ces informations à la base des victimes
    victimes = pd.merge(victimes, crois, how="left", on="id_vehicule")
    # Retrouve les noms de colonne initiaux pour les colonnes qui sont
    # doublonnées par le merge
    victimes.rename(columns={'Num_Acc_x':'Num_Acc',
                             'num_veh_x':'num_veh'},
                    inplace=True)

    # Récupère le véhicule en face pour les non-piétons
    victimes.loc[victimes["face_vehicule"].isnull(),
                 "face_vehicule"] = \
      victimes.loc[victimes["face_vehicule"].isnull(),
                   "face_vehicule_a"]

    # Et transmet dans la colonne face_catvsimple quand la valeur
    # n'est pas déjà définie (pour les piétons, elle l'est déjà et il
    # faut la conserver).
    transmet = victimes["face_catvsimple"].isnull()
    victimes.loc[transmet, "face_catvsimple"] = \
        victimes.loc[transmet, 'face_catvsimple_a']

    # Pour les piétons, il faut donner une valeur à face_liste et
    # face_multiple et poids
    transmet = victimes["catvsimple"] == "P"
    victimes.loc[transmet, "face_liste"] = \
        victimes.loc[transmet, 'face_catvsimple']
    victimes.loc[transmet, ("face_multiple", "poids")] = ( 1, 1 )

    # Ce qui reste : les cas où il n'y a pas d'autre véhicule impliqué…
    victimes.loc[victimes["face_catvsimple"].isnull(),
                 ("face_catvsimple", "poids")] = ( "0", 1 )

    # Restriction aux blessés d'une région donnée :
    if region is not None:
        victimes = selectionne_region(victimes, annee, region)

    # Nettoyage : colonnes temporaires à enlever
    del victimes['face_vehicule_a']
    del victimes['face_catvsimple_a']
        
    return victimes
