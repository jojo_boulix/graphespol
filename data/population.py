
import pandas as pd
from data import Sources

"""Population française

Ce module permet d'utiliser la base de données de l'INSEE contenant
les populations des communes de France en 2018.

Voir https://www.insee.fr/fr/statistiques/4989724?sommaire=4989761

Pour les collectivités d'outre-mer, les données disparates de l'INSEE
ont été regroupées à la main dans le fichier OutreMer.csv à partir des
données répertoriées sur la page
https://www.insee.fr/fr/statistiques/4989710?sommaire=4989761
Elles correspondent à des années différentes, entre 2017 et 2019.
"""

sources = Sources(__name__)

sources.enregistre("ensemble.zip",
  "https://www.insee.fr/fr/statistiques/fichier/4989724/ensemble.zip")

sources.enregistre("Communes.csv",
  {"type":"zip", "src":"ensemble.zip", "fichier":"Communes.csv"})

sources.enregistre("OutreMer.csv",
  {"type":"local",
   "src": [
       "https://www.insee.fr/fr/statistiques/4989710?sommaire=4989761"
   ]})

sources.enregistre("cog-2021.zip",
  "https://www.insee.fr/fr/statistiques/fichier/5057840/cog_ensemble_2021_csv.zip")

sources.enregistre("mvtcommune2021.csv",
  {"type":"zip", "src":"cog-2021.zip", "fichier":"mvtcommune2021.csv"})

def population_2018():
    """Données de population 2018

    Retour
    ------
    Une DataFrame contenant les population légales des communes de
    France issues des données INSEE pour 2018 ou 2017-2019
    (outre-mer), avec les colonnes suivantes :

    * INSEE pour le code INSEE en 2021 de la commune ou de
      l'arrondissement pour Paris, Lyon et Marseille

    * CODDEP pour le code de département

    * COM pour le nom de la commune

    * PMUN pour la population municipale

    """
    
    x = pd.read_csv(sources.fichier_joint("Communes.csv"),
                    dtype={'DEPCOM':str, 'CODCOM':str},
                    sep=";")

    # Fabrique le code INSEE à partir de CODDEP (code département à 2
    # ou 3 caractères) et de CODCOM (code commune, nombre à 3
    # chiffres)

    x["INSEE"] = x["CODDEP"].str[0:2] + x["CODCOM"]

    # Lecture des données pour l'Outre-Mer

    y = pd.read_csv(sources.fichier_joint("OutreMer.csv"),
                    dtype={'INSEE':str}, sep=",")

    y["CODDEP"] = y["INSEE"].str[0:3]

    return pd.concat([x, y])

def pre_regroupe(df, pop,
                 groupe, nom_groupe,
                 parties,
                 col_insee="INSEE", strict=False, force=False):
    """Regroupe des codes INSEE ensemble

    Dans le cas de Paris, Lyon et Marseille, on peut rencontrer des
    données basées sur les codes INSEE des arrondissements, ou bien
    des données basées sur les codes INSEE des communes.

    Les données de population disponibles sont basées sur les
    arrondissements de ces trois villes. Dans le cas où les données
    auxquelles on veut les associer sont basées sur les codes
    communes, il faut donc rassembler les arrondissements en une
    commune dans la base population. C'est l'objectif de cette
    fonction.

    Paramètres
    ----------
    df : DataFrame
      Les données en entrée, contenant un code INSEE de communes ou
      arrondissements.

    pop : DataFrame
      La base des populations forunie par population_2018() ou
      équivalent.

    groupe : str
      Le code INSEE de la commune regroupant les arrondissements.

    nom_groupe : str
      Le nom de la commune.

    parties : array(str)
      La liste des codes INSEE des arrondissements.

    col_insee : str
      Le nom de la colonne de df qui contient les codes INSEE.

    strict : boolean
      Dans le cas où strict=True, une erreur sera provoquée si dans
      les données de df on trouve à la fois le code INSEE <groupe> et
      un des codes INSEE de <parties>.

    force : boolean
      Dans le cas où force=True, on regroupera de toute façon les
      codes INSEE en utilisant le code INSEE de la commune, même si
      les données de df ne contiennent que des codes
      d'arrondissements.

    Side effects
    ------------
    Les deux DataFrame df et pop sont modifiées afin de tenir compte
    de la transformation éventuelle des codes INSEE arrondissements et
    codes INSEE commune

    Si un code <groupe> apparaît dans df, ou si <force> est vrai, 

    * tous les codes de <parties> sont transformés en <groupe> dans df.

    * les populations des <parties> sont effacées de pop, et remplacée
      par une ligne donnant la population totale de <groupe>

    """
    # Est-ce que le code <groupe> apparaît dans df ? Dans ce cas il
    # faudra regrouper…
    regroupe = (df[col_insee]==groupe).any()
    # Est-ce qu'un des codes de <parties> apparaît dans df ?
    separe = df[col_insee].isin(parties).any()
    # Si on a un mélange des deux cas, et si <strict> est vrai : on
    # produit une erreur.
    if separe and regroupe and strict:
        raise KeyError(f"Codes INSEE à la fois pour {groupe} et ses parties")

    # Regroupements
    if regroupe or force:
        # On calcule la population totale
        population_totale = sum(pop.loc[ pop["INSEE"].isin(parties),
                                         "PMUN" ])
        # On enlève les lignes de pop correspondant aux <parties>
        pop.drop(pop[ pop["INSEE"].isin(parties) ].index, inplace=True)
        # Et on ajoute une ligne pour <groupe> dans pop
        i = len(pop.index)
        pop.loc[i, ('INSEE','COM','CODDEP','PMUN')] = \
            [groupe, nom_groupe, groupe[0:2], population_totale]

        # Enfin, on transforme les codes INSEE dans df
        df.loc[ df[col_insee].isin(parties), col_insee ] = groupe

def insee_date(df, inconnus, from_date=2000, to_date=2021,
               col="INSEE", trace=False):
    """Chamgement de codes INSEE des communes au cours du temps

    Intègre à df les modification de code INSEE dûs à des
    regroupements de communes pour obtenir des codes INSEE à la date
    to_date.

    Paramètres
    ----------
    df : DataFrame
      Les données de départ.

    inconnus : 
      La liste des codes INSEE qui sont inconnus à la date to_date, et
      qu'il faut donc chercher à modifier.

    from_date : str
      Une date antérieure aux données (au format YYY-MM-DD).

    to_date : str
      La date à laquelle on veut obtenir les codes INSEE.

    col : str
      Le nom de la colonne de df qui contient les codes INSEE.

    trace : boolean
      Dans le cas où trace=True, une ligne sera écrite pour tout
      changement de code INSEE.

    Side effects
    ------------
    La colonne col de df sera modifiée en conséquence.

    """

    # Lecture des mouvements de communes
    mvt = pd.read_csv(sources.fichier_joint("mvtcommune2021.csv"),
                      dtype={'COM_AV':str, 'COM_AP':str}, sep=",")
    # Ne garde que cex qui concernent les communes (et pas les
    # communautés de communes, etc.), postérieurs à from_date et
    # antérieurs à to_date
    mvt = mvt[ (mvt["TYPECOM_AV"]=="COM") & (mvt["TYPECOM_AP"]=="COM")
               & (mvt["COM_AV"] != mvt["COM_AP"] )
               & (mvt["DATE_EFF"] >= str(from_date))
               & (mvt["DATE_EFF"] < str(to_date))
              ]
    # Tri par date croissante
    mvt.sort_values('DATE_EFF', ascending=True, inplace=True)
    # Transforme les codes INSEE
    for i,t in mvt.iterrows():
        if trace:
            if (inconnus & (df[col]==t['COM_AV'])).any():
                print(f"{t['COM_AV']} → {t['COM_AP']}")
        df.loc[ inconnus & (df[col]==t['COM_AV']),
                  col ] = t['COM_AP']


def merge_population(df, pop,
                     col_insee="INSEE",
                     from_date = 2010, to_date = 2021, trace_insee=True,
                     force_regroupe=False):
    """Intègre des données de population à une DataFrame

    Essaye de s'adapter à des données pour y incorporer des données de
    population 2018.

    Paramètres
    ----------
    df : DataFrame
      Les données en entrée, contenant un code INSEE pour identifier
      des communes ou arrondissements.

    pop : DataFrame
      La base des populations forunie par population_2018() ou
      équivalent.

    col_insee : str
      Le nom de la colonne de df qui contient les codes INSEE.

    from_date : str
      Une date antérieure aux données de df.

    to_date : str
      Une date postérieure aux codes INSEE présents dans pop (dans le
      cas de l'utilisation de population_2018(), il faut donc laisser
      2021).

    force_regroupe : boolean
      Doit-on forcément regrouper les arrondissements en communes pour
      Paris, Lyon et Marseille dans df et pop ?

    Retour
    ------
    Une DataFrame ajoutant les colonnes de population aux colonnes de
    df.

    Side effects
    ------------
    Les deux DataFrame df et pop ont été éventuellement modifiées pour
    regrouper les arrondissements en communes (voir pre_regroupe) et
    pour prendre en compte les changements de code INSEE au cours du
    temps, jusqu'à to_date.

    """
    # 1) Intègre à df les modification de code INSEE dûs à des
    # regroupements de communes pour obtenir des codes INSEE 2021, qui
    # correspondent à ceux existants dans la base de données de
    # population.
    #
    # On ne cherche pas à modifier les codes connus, qui peuvent
    # apparaître dans la liste des mouvements comme dans l'exemple de
    # 91122 (Création des Ulis à partir de bouts d'autres communes).

    insee_connus = pop["INSEE"]
    inconnus = ~df[col_insee].isin(insee_connus)

    insee_date(df, inconnus, from_date=from_date, to_date=to_date,
               col=col_insee, trace=trace_insee)

    # 2) Regroupement éventuel des arrondissements dans df et pop
    pre_regroupe(df, pop,
                 "75056", "Paris",
                 list(map(str, range(75101, 75121))),
                 col_insee=col_insee, force=force_regroupe)
    pre_regroupe(df, pop,
                 "69123", "Lyon",
                 list(map(str, range(68381, 68390))),
                 col_insee=col_insee, force=force_regroupe)
    pre_regroupe(df, pop,
                 "13055", "Marseille",
                 list(map(str, range(13201, 13217))),
                 col_insee=col_insee, force=force_regroupe)

    return pd.merge(df, pop, how="left",
                    left_on=col_insee, right_on="INSEE")
